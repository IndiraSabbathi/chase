package com.example.chase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    String transactionID;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        Bundle bundle = new Bundle();
        bundle.putString("msg_from_browser", "Launched from Browser");
        intent.putExtras(bundle);
        System.out.println(intent.toUri(Intent.URI_INTENT_SCHEME));
        Log.d("intent link-->", intent.toUri(Intent.URI_INTENT_SCHEME));
    }

    /** This method post the data from chase bank to MC and open the user consent page which is also provided by the MC
     * for example the url is the post service to send the data to MC once its receives success message from MC onResponse chase opens user
     * concert page which is also provided by MC here its "http://192.168.0.4:8080/userConcent.html?transctionID" this url brings user to user consent screen
     * there user approves or disapproves to the data to sent back to amazon  */
    public void submit(View view) throws JSONException {
       // final String body = "{age:25, DOB:09-09-90, transactionID:1234}";
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("age", "25");
        jsonParam.put("dob", "09-09-9090");
        jsonParam.put("transactionID", "123456789");
        jsonParam.put("dataFields", "");
        final String body = jsonParam.toString();
        String url = "http://192.168.0.4:8080/tp/ageValidation/post";
       // String url = "http://192.168.0.4:8100/data"; //mc
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String htmlContent) {
                Toast.makeText(getApplicationContext(), htmlContent.toString(), Toast.LENGTH_LONG).show();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.168.0.4:8080/userConcent.html?transctionID")); //mc
                startActivity(browserIntent);

                // web view Code starts here
               /*Button button = (Button) findViewById(R.id.button1);
                button.setVisibility(View.GONE);
                webView = (WebView) findViewById(R.id.webview);
                webView.setWebViewClient(new WebViewClient(){
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        handleNewUrl(url);
                        return true;
                    }
                });
                //webView.loadUrl("http://192.168.0.4:8080/index.html?transactionID=1234");
                webView.loadUrl("http://192.168.0.4:8080/userConcent.html?transactionID=1234");
                WebSettings webSettings = webView.getSettings();
                webSettings.setJavaScriptEnabled(true);*/
                // webview code ends here


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){
            @Override
            public String getBodyContentType() {return "application/json; charset-8";}

            @Override
            public byte[] getBody() throws AuthFailureError {
            try{
                return body == null? null : body.getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
            }
        };
        requestQueue.add(stringRequest);
    }

    // Post request after authentication

    public void postRequest(View view){
        try{
            String url = "http://192.168.0.4:8080/tp/ageValidation/post";
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            System.out.println("conn here :"+conn);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.connect();
            StringBuilder sbParams = new StringBuilder();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("transactionID", "1234");
            params.put("age", "25");
            int i = 0;
            for (String key : params.keySet()) {
                try {
                    if (i != 0){
                        sbParams.append("&");
                    }
                    sbParams.append(key).append("=")
                            .append(URLEncoder.encode(params.get(key), "UTF-8"));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                i++;
            }

            String paramsString = "hello";
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(paramsString);
            wr.flush();
            wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleNewUrl(String url) {
        Uri uri = Uri.parse(url);
        if (uri.getScheme().equals("http") || uri.getScheme().equals("https"))
            openExternalWebsite(url);
        else if(uri.getScheme().equals("app")){
            runApp("Amazon", uri);
        }
        else if (uri.getScheme().equals("intent") ) {
            String appPackage = getAppPackageFromUri(uri);

            if (appPackage != null) {
                PackageManager manager = getPackageManager();
                Intent appIntent = manager.getLaunchIntentForPackage(appPackage);

                if (appIntent != null) {
                    startActivity(appIntent);
                    //getActivity().startActivity(appIntent);
                } else {
                    openExternalWebsite("https://play.google.com/store/apps/details?id=" + appPackage);
                }
            }
        }
    }

    public void runApp(String appName, Uri uri) throws IllegalArgumentException {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.valueOf(uri)));
        startActivity(intent);
       /* Intent mainIntent = new Intent(String.valueOf(uri));
        System.out.println("mainIntent ====>"+mainIntent);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        for ( ResolveInfo info : getPackageManager().queryIntentActivities( mainIntent, 0) ) {
            System.out.println(info);
            if ( info.loadLabel(getPackageManager()).equals(appName) ) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(info.activityInfo.applicationInfo.packageName);
                startActivity(launchIntent);
                return;
            }
        }
        throw new IllegalArgumentException("Application not found!");*/
    }
    private void openExternalWebsite(String url) {
        Intent webeIntent = new Intent(Intent.ACTION_VIEW);
        webeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        webeIntent.setData(Uri.parse(url));
        startActivity(webeIntent);
    }

    private String getAppPackageFromUri(Uri intentUri) {
        Pattern pattern = Pattern.compile("package=(.*?);");
        Matcher matcher = pattern.matcher(intentUri.getFragment());

        if (matcher.find())
            return matcher.group(1);

        return null;
    }


}
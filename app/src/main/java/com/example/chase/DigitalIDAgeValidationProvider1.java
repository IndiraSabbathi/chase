package com.example.chase;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class DigitalIDAgeValidationProvider1 extends ContentProvider {

    static final String PROVIDER_NAME = "com.example.chase.DigitalIDAgeValidationProvider1";
    static final String URL = "content://"+PROVIDER_NAME+"/transactions";
    static final Uri CONTENT_URI = Uri.parse(URL);

    static final String _ID = "_Id";
    static final String _TRANSCTION_ID = "transactionID";
    static final String AGE = "age";

    private static HashMap<String, String> DIGITALID_PROJECTION_MAP;

    static final int DIGITALID = 1;

    static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "transactions", DIGITALID );
    }

    private SQLiteDatabase db;
    static final String DATABASE_NAME = "digitalID";
    static final String DIGITALID_TABLE_NAME = "transactionID";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE = "CREATE TABLE " + DIGITALID_TABLE_NAME +
                                            "( _Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                            "_TRANSCTION_ID TEXT NOT NULL, "+
                                            "AGE TEXT NOT NULL);";

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+DIGITALID_TABLE_NAME);
            onCreate(db);
        }
    }
    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null)?false:true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DIGITALID_TABLE_NAME);
        qb.setProjectionMap(DIGITALID_PROJECTION_MAP);
        qb.appendWhere("");
        if(sortOrder == null || sortOrder == ""){
            sortOrder = _TRANSCTION_ID;
        }

        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long rowID = db.insert(DIGITALID_TABLE_NAME, "", values);
        if(rowID >0){
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(uri, null);
            return _uri;
        }
        throw new SQLException("Failed to insert record "+uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
